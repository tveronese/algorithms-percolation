public class Percolation {

    private static final int TOP_SITE = 0;

    private static final int BOTTOM_SITE = 1;

    private final int n;

    private final boolean[][] grid;

    private final WeightedQuickUnionUF uf;

    /**
     * create N-by-N grid, with all sites blocked
     * 
     * @param n
     */
    public Percolation(int n) {

        this.n = n;
        this.grid = new boolean[n][n];
        this.uf = new WeightedQuickUnionUF(n * n + 2);

        for (int j = 0; j < n; j++) {
            this.uf.union(TOP_SITE, getIndex(0, j));
            this.uf.union(BOTTOM_SITE, getIndex(n - 1, j));
        }
    }

    /**
     * open site (row i, column j) if it is not already
     * 
     * @param i
     * @param j
     */
    public void open(int i, int j) {

        checkBounds(--i, --j);

        if (isOpen(i + 1, j + 1)) {
            return;
        }

        this.grid[i][j] = true;

        int siteIndex = getIndex(i, j);
        connect(siteIndex, i - 1, j);
        connect(siteIndex, i + 1, j);
        connect(siteIndex, i, j - 1);
        connect(siteIndex, i, j + 1);
    }

    /**
     * Connects two sites iif 0 <= i & j < n and the second site is open.
     * 
     * @param firstSite First site.
     * @param i Second site column.
     * @param j Second site row.
     */
    private void connect(int firstSite, int i, int j) {

        if (isInsideBounds(i, j) && isOpen(i + 1, j + 1)) {

            int secondSite = getIndex(i, j);
            this.uf.union(firstSite, secondSite);
        }
    }

    /**
     * is site (row i, column j) open?
     * 
     * @param i
     * @param j
     * @return
     */
    public boolean isOpen(int i, int j) {

        checkBounds(--i, --j);
        return this.grid[i][j];
    }

    /**
     * is site (row i, column j) full?
     * 
     * @param i
     * @param j
     * @return
     */
    public boolean isFull(int i, int j) {

        checkBounds(--i, --j);
        return isOpen(i + 1, j + 1) && this.uf.connected(TOP_SITE, getIndex(i, j));
    }

    /**
     * does the system percolate?
     * 
     * @return
     */
    public boolean percolates() {

        return this.uf.connected(TOP_SITE, BOTTOM_SITE);
    }

    private void checkBounds(int i, int j) {

        if (!isInsideBounds(i, j)) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private boolean isInsideBounds(int i, int j) {

        return i >= 0 && i < this.n && j >= 0 && j < this.n;
    }

    /**
     * Return the index of the (i, j) pair in the UF.
     * 
     * @param i
     * @param j
     * @return
     */
    private int getIndex(int i, int j) {

        return 2 + j + i * this.n;
    }

}
