public class PercolationStats {

    private final int n;

    private final int t;

    private double totalSites;

    private double mean;

    private double stddev;

    private double confidenceHi;

    private double confidenceLo;

    /**
     * perform T independent computational experiments on an N-by-N grid
     * 
     * @param n
     * @param t
     */
    public PercolationStats(int n, int t) {

        this.n = n;
        this.t = t;

        this.totalSites = n * n;

        this.calculate();
    }

    private void calculate() {

        double[] means = new double[this.t];
        for (int c = 0; c < this.t; c++) {

            Percolation percolation = new Percolation(this.n);

            int openedSites = 0;
            while (!percolation.percolates()) {
                int i = StdRandom.uniform(this.n) + 1;
                int j = StdRandom.uniform(this.n) + 1;

                // System.out.format("x = %d - opening (%d,%d)%n", ++x, i, j);
                if (!percolation.isOpen(i, j)) {
                    percolation.open(i, j);
                    openedSites++;
                }
            }

            means[c] = openedSites / this.totalSites;
        }

        this.mean = StdStats.mean(means);
        this.stddev = StdStats.stddev(means);

        double confidenceCoefficient = 1.96 * this.stddev / Math.sqrt(this.t);
        this.confidenceLo = this.mean - confidenceCoefficient;
        this.confidenceHi = this.mean + confidenceCoefficient;
    }

    /**
     * sample mean of percolation threshold
     * 
     * @return
     */
    public double mean() {

        return this.mean;
    }

    /**
     * sample standard deviation of percolation threshold
     * 
     * @return
     */
    public double stddev() {

        return this.stddev;
    }

    /**
     * returns lower bound of the 95% confidence interval
     * 
     * @return
     */
    public double confidenceLo() {

        return this.confidenceLo;
    }

    /**
     * returns upper bound of the 95% confidence interval
     * 
     * @return
     */
    public double confidenceHi() {

        return this.confidenceHi;
    }

    /**
     * test client
     * 
     * @param args
     */
    public static void main(String[] args) {

        int n = Integer.valueOf(args[0]);
        int t = Integer.valueOf(args[1]);

        // Stopwatch stopwatch = new Stopwatch();
        PercolationStats stats = new PercolationStats(n, t);
        // double elapsedTime = stopwatch.elapsedTime();
        // System.out.format("elapsed time = %f%n%n", elapsedTime);

        System.out.format("mean\t\t\t= %.20f%n", stats.mean());
        System.out.format("stddev\t\t\t= %.20f%n", stats.stddev());
        System.out.format("95%% confidence interval\t= %.20f, %.20f %n", stats.confidenceLo(), stats.confidenceHi());
    }

}
