import static com.googlecode.catchexception.CatchException.verifyException;
import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

public class PercolationTest {

    @Test
    public void shouldPercolate() {

        Percolation p = new Percolation(3);
        assertThat(p.percolates()).isFalse();
        assertThat(p.isFull(1, 1)).isFalse();

        p.open(1, 2);
        assertThat(p.isFull(1, 2)).isTrue();
        assertThat(p.percolates()).isFalse();

        p.open(2, 2);
        assertThat(p.isFull(2, 2)).isTrue();
        assertThat(p.percolates()).isFalse();

        p.open(2, 2);
        assertThat(p.isFull(2, 2)).isTrue();
        assertThat(p.percolates()).isFalse();

        p.open(3, 3);
        assertThat(p.isFull(3, 3)).isFalse();
        assertThat(p.percolates()).isFalse();

        p.open(2, 1);
        assertThat(p.isFull(2, 1)).isTrue();
        assertThat(p.percolates()).isFalse();

        p.open(2, 1);
        assertThat(p.isFull(1, 2)).isTrue();
        assertThat(p.percolates()).isFalse();

        p.open(3, 1);
        assertThat(p.isFull(3, 1)).isTrue();
        assertThat(p.percolates()).isTrue();

        // Backwash test
        // assertThat(p.isFull(3, 3)).isFalse();
        // assertThat(p.percolates()).isFalse();
    }

    @Test
    public void shouldThrowArrayOutOfBoundsException() {

        Percolation p = new Percolation(3);
        verifyException(p, ArrayIndexOutOfBoundsException.class).open(0, 1);
        verifyException(p, ArrayIndexOutOfBoundsException.class).open(1, 0);
        verifyException(p, ArrayIndexOutOfBoundsException.class).open(4, 1);
        verifyException(p, ArrayIndexOutOfBoundsException.class).open(1, 4);

        assertThat(p.percolates()).isFalse();
    }

}
